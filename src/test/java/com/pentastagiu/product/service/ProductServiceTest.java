package com.pentastagiu.product.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.pentastagiu.product.dto.ProductDTO;
import com.pentastagiu.product.exceptions.ProductNotFoundException;
import com.pentastagiu.product.model.Product;
import com.pentastagiu.product.repository.ProductRepository;
import com.pentastagiu.utils.Response;

@RunWith(SpringRunner.class)
public class ProductServiceTest {

	@Mock
	ProductRepository productRepository;

	@InjectMocks
	ProductService productService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testEditProductShouldReturnOk() {
		when(productRepository.findById(Mockito.any())).thenReturn(Optional.of(new Product()));
		ResponseEntity<Response<Product>> response = productService.editProduct((long) 1, new ProductDTO());
		assertEquals(response.getStatusCode().toString(), HttpStatus.ACCEPTED.toString());
	}

	@Test(expected = ProductNotFoundException.class)
	public void testEditProductShouldReturnBadRequest() {
		when(productRepository.findById(Mockito.any())).thenReturn(Optional.empty());
		productService.editProduct((long) 1, new ProductDTO());
	}
}
