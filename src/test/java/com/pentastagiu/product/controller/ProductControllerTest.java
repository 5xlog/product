package com.pentastagiu.product.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.pentastagiu.product.dto.ProductDTO;
import com.pentastagiu.product.exceptions.ProductNotFoundException;
import com.pentastagiu.product.handler.RestControllerAdvice;
import com.pentastagiu.product.repository.ProductRepository;
import com.pentastagiu.product.service.ProductService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductControllerTest {

	private MockMvc mockMvc;

	@Mock
	ProductService productService;

	@Mock
	ProductRepository productRepository;

	@InjectMocks
	ProductController productController;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(productController).setControllerAdvice(new RestControllerAdvice())
				.build();
	}

	@Test
	public void testEditProductShouldReturnOK() throws Exception {
		ProductDTO product = new ProductDTO();
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		when(productService.editProduct(Mockito.any(), Mockito.any()))
				.thenReturn(ResponseEntity.status(HttpStatus.ACCEPTED).body(null));
		mockMvc.perform(
				put("/product/{id}", 1).contentType(MediaType.APPLICATION_JSON).content(ow.writeValueAsString(product)))
				.andExpect(status().isAccepted());
	}

	@Test
	public void testEditProductShouldThrowNotFoundException() throws JsonProcessingException, Exception {
		ProductDTO product = new ProductDTO();
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		when(productService.editProduct(Mockito.any(), Mockito.any())).thenThrow(ProductNotFoundException.class);
		mockMvc.perform(
				put("/product/{id}", 1).contentType(MediaType.APPLICATION_JSON).content(ow.writeValueAsString(product)))
				.andExpect(status().isNotFound());
	}

}
