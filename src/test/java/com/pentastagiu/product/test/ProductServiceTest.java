package com.pentastagiu.product.test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.pentastagiu.product.repository.ProductRepository;
import com.pentastagiu.product.service.ProductService;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ProductServiceTest {

	@InjectMocks
	private ProductService productService;

	@Mock
	private ProductRepository productRepository;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(productService);
	}
	
	
	@Test
	public void checkAvailabilityShouldReturnTrue() {
		Long id=1l;
		when(productRepository.existsById(Mockito.any())).thenReturn(true);
		boolean productExists = productService.checkAvailability(id);
		
		assertThat(productExists,is(true));
	}
	
}
