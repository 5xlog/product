package com.pentastagiu.product.model;

import java.time.LocalDateTime;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import com.pentastagiu.product.enums.Rating;

@Entity(name = "review")
public class Review {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "user_id")
	private Long userId;

	@ManyToOne()
	@JoinColumn(name = "product_id")
	private Product reviewedProduct;

	@Column(name = "rating")
	@Enumerated(EnumType.STRING)
	private Rating rating;

	@Column(name = "body")
	@Lob
	@Basic(fetch = FetchType.LAZY)
	private String body;

	@Column(name = "submit_date")
	private LocalDateTime submitDate;

	// Getters & Setters
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Product getreviewedProduct() {
		return reviewedProduct;
	}

	public void setreviewedProduct(Product reviewedProduct) {
		this.reviewedProduct = reviewedProduct;
	}

	public Rating getRating() {
		return rating;
	}

	public void setRating(Rating rating) {
		this.rating = rating;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public LocalDateTime getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(LocalDateTime submitDate) {
		this.submitDate = submitDate;
	}

	public Long getId() {
		return id;
	}

}
