package com.pentastagiu.product.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pentastagiu.product.dto.ProductDTO;
import com.pentastagiu.product.model.Product;
import com.pentastagiu.product.service.ProductService;
import com.pentastagiu.utils.Response;

/**
 * Controller for operations with products
 * 
 * @author Vacariuc Bogdan
 *
 */
@RestController
public class ProductController {

	@Autowired
	ProductService productService;

	/**
	 * Edits an existing object from the database. If the object with the requested
	 * id is not in the database it return bad request.
	 * 
	 * @param id         id of the product
	 * @param productDTO informations that have to be changed
	 * @return ResponseEntity of Response which contains response statuses. data
	 *         field is null
	 */
	@PutMapping(value = "/product/{id}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response<Product>> editProduct(@PathVariable(value = "id") Long id,
			@RequestBody ProductDTO productDTO) {
		return productService.editProduct(id, productDTO);
	}
	
	
	@DeleteMapping(path = "/product", produces = "application/json")
	public ResponseEntity<Response<String>> deleteProduct(@RequestParam Long id) {

		Response<String> response = new Response<>();
		response.addResponseStatus(productService.deleteProduct(id));

		return ResponseEntity.ok(response);
	}
}
