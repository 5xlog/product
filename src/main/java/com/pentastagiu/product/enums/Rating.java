package com.pentastagiu.product.enums;

public enum Rating {
	NO_STARS,
	ONE_STAR,
	TWO_STARS,
	THREE_STARS,
	FOUR_STARS,
	FIVE_STARS;
}
