package com.pentastagiu.product.enums;

public enum Availability {
	AVAILABLE, NOT_AVAILABLE;
}
