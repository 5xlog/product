package com.pentastagiu.product.enums;

public final class ResponseStatusMessage {
	public static final String SUCCESS_VALIDATION_MESSAGE = "The product was successfully deleted";
	public static final String ERROR_VALIDATION_MESSAGE = "The product wasn't found";
}
