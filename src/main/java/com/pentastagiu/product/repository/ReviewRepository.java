package com.pentastagiu.product.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pentastagiu.product.model.Review;

@Repository
public interface ReviewRepository extends JpaRepository<Review,Long> {

}
