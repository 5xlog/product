package com.pentastagiu.product.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.pentastagiu.product.model.Product;

/**
 * Repository class for persisting objects of Product
 * 
 * @author Vacariuc Bogdan
 *
 */
@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

	Product findOneById(Long id);

}
