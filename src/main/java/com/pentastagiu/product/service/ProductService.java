package com.pentastagiu.product.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.pentastagiu.product.dto.ProductDTO;
import com.pentastagiu.product.enums.Availability;
import com.pentastagiu.product.enums.ResponseStatusMessage;
import com.pentastagiu.product.enums.Status;
import com.pentastagiu.product.exceptions.ProductNotFoundException;
import com.pentastagiu.product.model.Product;
import com.pentastagiu.product.repository.ProductRepository;
import com.pentastagiu.utils.Response;
import com.pentastagiu.utils.ResponseStatus;

/**
 * Service class for operations with products
 * 
 * @author Vacariuc Bogdan
 *
 */
@Service
public class ProductService {

	private ProductRepository productRepository;

	@Autowired
	public ProductService(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	/**
	 * Finds the object in the database by the given id. If it is found, updates the
	 * object with the given informations.
	 * 
	 * @param id         id of the object that has to be updated
	 * @param productDTO mapped informations that has to be updated to the Product
	 *                   object
	 * @return ResponseEntity that contains OK response with the body containing an
	 *         ok message if the object was found, BAD_REQUEST if the object was not
	 *         found. Field data is null in both cases.
	 */
	public ResponseEntity<Response<Product>> editProduct(Long id, ProductDTO productDTO) {
		Optional<Product> optionalProduct = productRepository.findById(id);
		if (optionalProduct.isPresent()) {
			Product product = optionalProduct.get();
			if (productDTO.getDescription() != null) {
				product.setDescription(productDTO.getDescription());
			}
			if (productDTO.getName() != null) {
				product.setName(productDTO.getName());
			}
			if (productDTO.getPrice() != null) {
				product.setPrice(productDTO.getPrice());
			}
			if (productDTO.getStock() != null) {
				product.setStock(productDTO.getStock());
			}
			productRepository.save(product);
			ResponseStatus responseStatus = new ResponseStatus("Product changed", Status.OK);
			Response<Product> response = new Response<Product>(null, responseStatus);
			return ResponseEntity.accepted().body(response);
		} else {
			throw new ProductNotFoundException("Product not found");
		}
	}
	
	public boolean checkAvailability(Long id) {
		if (productRepository.existsById(id)) {
			return true;
		}
		return false;
	}
	
	public ResponseStatus deleteProduct(Long id) {
		ResponseStatus responseStatus = new ResponseStatus();
		Product product = productRepository.findOneById(id);
		if (checkAvailability(id)) {
			product.setAvailability(Availability.NOT_AVAILABLE);
			productRepository.save(product);
			responseStatus.setStatus(Status.OK);
			responseStatus.setMessage(ResponseStatusMessage.SUCCESS_VALIDATION_MESSAGE);
		} else {
			responseStatus.setStatus(Status.ERROR);
			responseStatus.setMessage(ResponseStatusMessage.ERROR_VALIDATION_MESSAGE);
		}
		return responseStatus;

	}
}
