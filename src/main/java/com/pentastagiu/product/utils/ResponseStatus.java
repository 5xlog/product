package com.pentastagiu.product.utils;

import com.pentastagiu.product.enums.Status;

public class ResponseStatus {

	private String message;
	private Status status;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
}
