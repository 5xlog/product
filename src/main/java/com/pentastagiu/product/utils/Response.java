package com.pentastagiu.product.utils;

import java.util.List;

public class Response<T extends Object> {

	private T data;
	private List<ResponseStatus> responseStatuses;

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public List<ResponseStatus> getResponseStatuses() {
		return responseStatuses;
	}

	public void setResponseStatuses(List<ResponseStatus> responseStatuses) {
		this.responseStatuses = responseStatuses;
	}
}
