package com.pentastagiu.product.handler;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.pentastagiu.product.enums.Status;
import com.pentastagiu.product.exceptions.ProductNotFoundException;
import com.pentastagiu.product.model.Product;
import com.pentastagiu.utils.Response;
import com.pentastagiu.utils.ResponseStatus;

/**
 * Controller advice used for ProductController
 * 
 * @author Vacariuc Bogdan
 *
 */
@ControllerAdvice
public class RestControllerAdvice {

	/**
	 * When the ProductNotFoundException is encountered puts the response on
	 * NOT_FOUND status and bodies the response
	 * 
	 * @param request
	 * @param ex
	 * @return
	 */
	@ExceptionHandler(ProductNotFoundException.class)
	public ResponseEntity<Response<Product>> handleAllExceptions(HttpServletRequest request, Exception ex) {
		ResponseStatus responseStatus = new ResponseStatus(ex.getMessage(), Status.ERROR);
		Response<Product> response = new Response<Product>(null, responseStatus);
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
	}
}
