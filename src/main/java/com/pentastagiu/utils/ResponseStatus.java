package com.pentastagiu.utils;

import com.pentastagiu.product.enums.Status;
/**
 * Class used in the response that holds informations about the eventual errors
 * that occurred. A response can contain more that one ResponseStatus.
 * 
 * @author Vacariuc Bogdan
 *
 */
public class ResponseStatus {
	private String message;
	private Status status;

	public ResponseStatus(String message, Status errorStatus) {
		this.message = message;
		this.status = errorStatus;
	}

	public ResponseStatus() {
		// TODO Auto-generated constructor stub
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}
