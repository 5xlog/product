package com.pentastagiu.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Generic class used for responses in the controller class.
 * 
 * @author Vacariuc Bogdan
 *
 * @param <T> object that has to be sent as data
 */
public class Response<T extends Object> {
	private T data;
	private List<ResponseStatus> responseStatuses;

	public Response() {
		responseStatuses = new ArrayList<ResponseStatus>();
	}

	public Response(T data, ResponseStatus responseStatus) {
		responseStatuses = new ArrayList<ResponseStatus>();
		this.data = data;
		responseStatuses.add(responseStatus);
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public List<ResponseStatus> getResponseStatuses() {
		return responseStatuses;
	}

	public void addResponseStatus(ResponseStatus responseStatus) {
		responseStatuses.add(responseStatus);
	}

	public void setResponseStatuses(List<ResponseStatus> responseStatuses2) {
		this.responseStatuses = responseStatuses2;
		
	}
}
